#+TITLE: Jonnobrow PC Kubernetes Cluster

* Creating Virtual Machines

** Creation Script
This script is basically a copy of the script from [[https://blog.alexellis.io/kvm-kubernetes-primer/][a blog post by Alex Ellis]].


#+begin_src shell :tangle create-vm.sh :exports code
#!/usr/bin/env bash

if [ -z "$1" ] ;
then
    echo "Specify a name for the virtual-machine"
    exit 1
fi

sudo virt-install \
    --name $1 \
    --ram 4096 \
    --disk path=/var/lib/libvirt/images/$1.img,size=30 \
    --vcpus 2 \
    --os-type generic \
    --os-variant ubuntu18.04 \
    --network bridge:br0,model=virtio \
    --graphics none \
    --console pty,target_type=serial \
    --location 'http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64' \
    --extra-args 'console=ttyS0,115200n8 serial'
#+end_src

** Running the Script

Again, you should probably just follow the guide in Alex Ellis' blog post linked above.

#+begin_src shell
./create-vm.sh kube-master   # Master Node
./create-vm.sh kube-w-node-1 # First worker node
#+end_src

* Network

** Network Bridge Systemd-networkd

- [[https://wiki.archlinux.org/index.php/Systemd-networkd#Bridge_interface][Arch Linux Guide]]

#+caption: /etc/systemd/network/br0.netdev
#+begin_src systemd :exports code
[NetDev]
Name=br0
Kind=Bridge
#+end_src

#+caption: /etc/systemd/network/bind.network
#+begin_src systemd :exports code
[Match]
Name=enp34s0

[Network]
Bridge=br0
#+end_src

#+caption: /etc/systemd/network/br0.network
#+begin_src systemd :exports code
[Match]
Name=br0

[Network]
DHCP=ipv4
#+end_src

* Installing Kubernetes (On the Guests)
